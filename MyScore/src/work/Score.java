package work;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.lang.model.element.Element;
import javax.lang.model.util.Elements;
import javax.swing.text.Document;

import org.jsoup.Jsoup;

public class Score {
	public static void main(String[] args) throws IOException {
		double program = 0;
		double before = 0;
		double base = 0;
		double add = 0;
		double test = 0;
		Properties prop = new Properties();
		// 读取属性文件a.properties
		InputStream in = new BufferedInputStream(new FileInputStream("total.properties"));
		prop.load(in); /// 加载属性列表
		Iterator<String> it = prop.stringPropertyNames().iterator();
		if (it.hasNext()) {
			String key = it.next();
			program = Integer.parseInt(prop.getProperty(key));
		}
		if (it.hasNext()) {
			String key = it.next();
			before = Integer.parseInt(prop.getProperty(key));
		}
		if (it.hasNext()) {
			String key = it.next();
			base = Integer.parseInt(prop.getProperty(key));
		}
		if (it.hasNext()) {
			String key = it.next();
			add = Integer.parseInt(prop.getProperty(key));
		}
		if (it.hasNext()) {
			String key = it.next();
			test = Integer.parseInt(prop.getProperty(key));
		}
		Scanner scanner = new Scanner(System.in);
		String small = scanner.next();
		String all = scanner.next();
		scanner.close();

		HashMap<String, Integer> map = getSectionTotal(small, all);

		totalScores(map, before, base, test, program, add);

	}

	private static void totalScores(HashMap<String, Integer> map, Double before, Double base, Double test,
			Double program, Double add) {
		Double totalScore = 0.0;
		// 计算课前自测成绩
		int bef = map.get("before");
		Double resBef = bef / before * 100 * 0.25;
		// 计算课堂完成部分成绩
		int bas = map.get("base");
		Double resBas = bas / base * 100 * 0.95 * 0.3;
		// 小测部分
		int tes = map.get("test");
		Double resTes = tes / test * 100 * 0.25;
		// 计算编程部分成绩
		int pro = map.get("program");
		Double resPro = pro / program * 100 * 0.1;
		if (resPro > 95) {
			resPro = (double) 95;
		}
		// 计算附加题
		int ad = map.get("add");
		Double resAd = ad / add * 100 * 0.05;
		if (resAd > 90) {
			resAd = (double) 90;
		}
		totalScore = resAd + resBas + resBef + resPro + resTes;
		System.out.println(totalScore);
	}

	private static HashMap<String, Integer> getSectionTotal(String small, String all) throws IOException {
		// 形成路径
		String filePath1 = "small.html";
		String filePath2 = "all.html";
		// 计算大班小班各模块的分数并返回给相应的集合
		java.util.HashMap<String, Integer> small1 = getScores(filePath1);
		java.util.HashMap<String, Integer> all1 = getScores(filePath2);
		java.util.HashMap<String, Integer> sum = new HashMap<>();
		// 遍历两个集合的值算出每个模块总分的总和
		String[] section = new String[5];
		for (int i = 0; i < section.length; i++) {
			section[i] = new String();
		}
		section[0] = "before";
		section[1] = "base";
		section[2] = "test";
		section[3] = "program";
		section[4] = "add";

		for (int i = 0; i < 5; i++) {

			int x = all1.get(section[i]);
			x += small1.get(section[i]);
			sum.put(section[i], x);
		}
		return sum;

	}

	private static HashMap<String, Integer> getScores(String filepath) throws IOException {
		org.jsoup.nodes.Document document = Jsoup.parse(new File(filepath), "utf-8");
		Integer before = 0;
		Integer base = 0;
		Integer test = 0;
		Integer program = 0;
		Integer add = 0;
		String regex1 = "课前自测";
		String regex2 = "课堂完成部分";
		String regex3 = "课堂小测";
		String regex4 = "编程题";
		String regex5 = "附加题";
		String experienceRegex = "\\d+";
		int experience = 0;
		org.jsoup.select.Elements interactionRow = document.getElementsByClass("interaction-row");
		for (int i = 0; i < interactionRow.size(); i++) {
			// 获取inertactionRow下所有子节点并取第一个子节点
			org.jsoup.select.Elements rowChild = interactionRow.get(i).children();
			// 获取interactionRow第2个子节点下的第二个子节点
			org.jsoup.select.Elements rowChild2 = rowChild.get(1).children();
			// 获取第二个节点下的第一个节点所有子节点
			org.jsoup.select.Elements rowChild2_1 = rowChild2.get(0).children();
			org.jsoup.nodes.Element section = rowChild2_1.get(1);
			// 获取 row_chile_2_3下第一个节点的所有子节点
			org.jsoup.select.Elements rowChild2_3 = rowChild2.get(2).children();
			org.jsoup.select.Elements rowChild2_3_1 = rowChild2_3.get(0).children();
			// 获取row_chile_2_3_1下第七个与第八个节点
			org.jsoup.nodes.Element span_7 = rowChild2_3_1.get(6);
			org.jsoup.nodes.Element span_8 = rowChild2_3_1.get(rowChild2_3_1.size() - 1);
			Pattern p = Pattern.compile(experienceRegex);
			Matcher matcher = p.matcher(span_8.text());
			if (matcher.find()) {
				experience = Integer.parseInt(matcher.group(0));

			}
			if (section.text().contains(regex1)) {
				before += experience;
			} else if (section.text().contains(regex2)) {

				base += experience;
			} else if (section.text().contains(regex3)) {
				test += experience;
			} else if (section.text().contains(regex4)) {
				program += experience;
			} else if (section.text().contains(regex5)) {
				add += experience;
			}
		}
		HashMap<String, Integer> sectionExperience = new HashMap<>();
		sectionExperience.put("before", before);
		sectionExperience.put("base", base);
		sectionExperience.put("test", test);
		sectionExperience.put("program", program);
		sectionExperience.put("add", add);

		return sectionExperience;

	}

}
